/* Copyright 2020 IFo Hancroft
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

  [0] = LAYOUT_ortho_5x15(
    KC_GRAVE,  KC_1,    KC_2,    KC_3,    KC_4,     KC_5,     KC_6,  KC_7,     KC_8,     KC_9,     KC_0,   KC_MINUS, KC_EQUAL, KC_PSCR,      KC_BSPC,
    KC_TAB,    MO(1),   KC_Q,    KC_W,    KC_E,     KC_R,     KC_T,  KC_Y,     KC_U,     KC_I,     KC_O,   KC_P,     KC_LBRC,  KC_RBRC,      KC_DELETE,
    KC_ESCAPE, MO(2),   KC_A,    KC_S,    KC_D,     KC_F,     KC_G,  KC_H,     KC_J,     KC_K,     KC_L,   KC_SCLN,  KC_QUOTE, KC_BACKSLASH, KC_ENTER,
    KC_LSFT,   MO(1),   KC_Z,    KC_X,    KC_C,     KC_V,     KC_B,  KC_N,     KC_M,     KC_COMMA, KC_DOT, KC_SLASH, KC_NO,    KC_UP,        KC_NO,
    KC_LCTL,   KC_LGUI, KC_LGUI, KC_LALT, KC_SPACE, KC_SPACE, KC_NO, KC_SPACE, KC_SPACE, KC_RALT,  KC_NO,  KC_NO,    KC_LEFT,  KC_DOWN,      KC_RIGHT
  ),
  [1] = LAYOUT_ortho_5x15(
    QK_BOOT, KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  KC_TRNS, KC_TRNS,
    BL_UP,   KC_TRNS, KC_BTN1, KC_PGUP, KC_BTN2, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_NUM,  KC_CAPS, KC_SCRL, KC_RALT, KC_INT2,
    TD(0),   KC_TRNS, KC_HOME, KC_PGDN, KC_END,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_BRIU,
    BL_DOWN, KC_TRNS, KC_WBAK, KC_TRNS, KC_WFWD, KC_TRNS, KC_TRNS, KC_TRNS, KC_MUTE, KC_TRNS, KC_TRNS, KC_TRNS, KC_MPLY, KC_VOLU, KC_BRID,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_NO,   KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_MPRV, KC_VOLD, KC_MNXT
  ),
  [2] = LAYOUT_ortho_5x15(
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_NUM_LOCK, KC_KP_SLASH, KC_KP_ASTERISK, KC_KP_MINUS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_KP_7,     KC_KP_8,     KC_KP_9,        KC_KP_PLUS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_KP_4,     KC_KP_5,     KC_KP_6,        KC_KP_COMMA,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_KP_1,     KC_KP_2,     KC_KP_3,        KC_KP_EQUAL,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_NO,   KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_KP_0,     KC_KP_0,     KC_KP_DOT,      KC_KP_ENTER
  ),
  [3] = LAYOUT_ortho_5x15(
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_NO,   KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS
  )
};

void keyboard_post_init_user(void) {
  // On tap | On hold | On double tap | On tap + hold | Tapping term (ms)
  vial_tap_dance_entry_t td1 = { RGB_MOD, KC_NO, KC_CAPS_LOCK, KC_NO, TAPPING_TERM };
  dynamic_keymap_set_tap_dance(0, &td1);
//  vial_tap_dance_entry_t td2 = { KC_CAPS_LOCK, MO(2), KC_NO, KC_NO, TAPPING_TERM };
//  dynamic_keymap_set_tap_dance(1, &td2);
};
