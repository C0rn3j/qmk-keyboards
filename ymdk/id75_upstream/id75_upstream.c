/* Copyright 2021 MT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "quantum.h"

#ifdef RGB_MATRIX_ENABLE
bool rgb_matrix_indicators_kb(void) {
	if (!rgb_matrix_indicators_user()) {
		return false;
	}

	// macOS does not have Num Lock
	if (host_keyboard_led_state().num_lock) {
		rgb_matrix_set_color(49, 0, 40, 0);   // ON = GREEN
	} else {
		rgb_matrix_set_color(63, 40, 0, 0);   // OFF = RED
		rgb_matrix_set_color(49, 40, 40, 0); // OFF = YELLOW
	}

	if (host_keyboard_led_state().caps_lock) {
		rgb_matrix_set_color(44, 40, 0, 0);   // ON = RED
		rgb_matrix_set_color(48, 0, 40, 0);   // ON = GREEN
	} else {
		rgb_matrix_set_color(48, 40, 40, 0); // OFF = YELLOW
	}

	// macOS does not have Scroll Lock
	// Linux has broken Scroll Lock on X and Wayland, direct is fine - test with for i in $(\ls /sys/class/leds | grep scrolllock); do echo 1 > "$(readlink -f "/sys/class/leds/$i")/brightness"; done
	//   https://gitlab.freedesktop.org/libinput/libinput/-/merge_requests/990
	if (host_keyboard_led_state().scroll_lock) {
		rgb_matrix_set_color(61, 40, 0, 0);   // ON = RED
		rgb_matrix_set_color(47, 0, 40, 0);   // ON = GREEN
	} else {
		rgb_matrix_set_color(47, 40, 40, 0); // OFF = YELLOW
	}

	// Linux does send Compose events - https://gitlab.freedesktop.org/libinput/libinput/-/merge_requests/990
	// Windows does not have a Compose key and wincompose project claims the API does not even have a function to toggle it
	// macOS probably lacks it
	if (host_keyboard_led_state().compose) {
		rgb_matrix_set_color(5, 0, 40, 0);    // ON = GREEN
		rgb_matrix_set_color(46, 0, 40, 0);   // ON = GREEN
	} else {
		rgb_matrix_set_color(46, 40, 40, 0); // OFF = YELLOW
	}

	// Linux does send Kana events - https://gitlab.freedesktop.org/libinput/libinput/-/merge_requests/990
	// Wincompose project claims Kana LED can be set by the API
	// macOS seems to have support for this, surprisingly? Unsure which keys, but source code dumps from old macOS have it
	if (host_keyboard_led_state().kana) {
		rgb_matrix_set_color(45, 0, 40, 0);   // ON = GREEN
	} else {
		rgb_matrix_set_color(45, 40, 40, 0); // OFF = YELLOW
	}

	return true;
}
#endif
