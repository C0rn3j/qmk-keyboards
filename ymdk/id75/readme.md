
---

**PLEASE NOTE THERE IS NOW UPSTREAM SUPPORT FOR THIS https://github.com/vial-kb/vial-qmk/tree/vial/keyboards/ymdk/id75**

*../id75_upstream has upstream(at the time) configs overriden with my things*

---

# Main

YMDK's "v3" attempt at Idobao's id75, but this time with a better (STM32F103-blue-pill-v0) MCU.

UF2 binaries from YMDK for VIA and VIAL are provided here.

VIAL compiles from upstream [vial-qmk](https://github.com/vial-kb/vial-qmk) after copying this directory in it:
```bash
git clone --depth 1 https://gitlab.com/C0rn3j/qmk-keyboards /tmp/qmk-keyboards
git clone --depth 1 https://github.com/vial-kb/vial-qmk /tmp/vial-qmk
cp -r /tmp/qmk-keyboards/ymdk/id75 /tmp/vial-qmk/keyboards/ymdk/
cd /tmp/vial-qmk
make ymdk/id75:vial
```

I was not able to compile the provided VIA files due to an EEPROM compile error.

Link to keyboard (may be expired): https://www.aliexpress.com/item/1005004763575742.html

Also see README.md of this repo, not just this dir.

# Use upstream with my changes

```bash
git clone --depth 1 https://gitlab.com/C0rn3j/qmk-keyboards /tmp/qmk-keyboards
git clone --depth 1 https://github.com/vial-kb/vial-qmk /tmp/vial-qmk
rsync -av --delete ~/Projects/qmk-keyboards/ymdk/id75_upstream /tmp/vial-qmk/keyboards/ymdk/
cd /tmp/vial-qmk && make ymdk/id75_upstream:vial && cd - && dolphin /tmp/vial-qmk
# Copy ymdk_id75_upstream_vial.uf2 over to the keyboard after RESETing it
```
