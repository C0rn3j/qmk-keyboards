/* SPDX-License-Identifier: GPL-2.0-or-later */
#pragma once

#define ENABLE_RGB_MATRIX_SOLID_MULTISPLASH
#define VIAL_KEYBOARD_UID {0xAE, 0xED, 0x0E, 0xDA, 0xD1, 0x21, 0xD5, 0x20 }
