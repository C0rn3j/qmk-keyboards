# qmk keyboards

This is a repo for my qmk_firmware keyboards

https://gitlab.com/m-lego/how/-/blob/main/how_matrix_works_extra.md


## Adding VIA support

1. Copy your default keymap directory as `via`
1. Add `rules.mk` file to it with `VIA_ENABLE = yes` as its contents
1. Compile and flash the keymap to your board
1. Now you need to create a design JSON for VIA - Follow VIAL's documentation on porting to VIA https://get.vial.today/docs/porting-to-via.html
1. Cross-check against VIA's specifications https://www.caniusevia.com/docs/specification
1. Save the resulting JSON as <something>.json
1. Go to VIA https://usevia.app/, enable Design tab, connect your keyboard and upload your JSON to your board
1. VIA should now be fully functional for your board

## Note to myself for updating

Current keymap is id75_upstream based on https://github.com/qmk/qmk_firmware/blob/master/keyboards/ymdk/id75 with an associated build README in ymdk/id75_upstream/readme.md
